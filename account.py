# encoding: UTF-8

class Account(object):

    def __init__(self, cardholder):
        self.cardholder = cardholder
        self.savings = 0
        self.balance = 0
