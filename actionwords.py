# encoding: UTF-8
from cardholder import Cardholder
from card import Card
from atm import ATM
from account import Account

class Actionwords:
    def __init__(self, test):
        self.test = test
        self.cardholder = Cardholder('foobar', 'foobar@mail.com')
        self.account = Account(self.cardholder)
        self.atm = ATM()
        self.message = ""
        self.requested_amount = 0

    def the_card_is_valid(self):
        self.atm.insert_card(Card('371520790975468', '340216050175729', '378703708118990'))

    def the_machine_contains_enough_money(self):
        self.atm.supply_cash_dispender(self.account.balance)

    def the_card_should_be_returned(self):
        self.atm.release_card()

    def the_account_balance_is_balance(self, balance = ""):
        balance = int(balance[1:])
        self.account.balance = balance

    def the_account_holder_requests_amount(self, amount = ""):
        self.atm.start_transaction(self.account)
        amount = int(amount[1:])
        self.requested_amount = amount

    def the_atm_should_dispense_amount(self, amount = ""):
        amount = int(amount[1:])
        self.atm.withdraw(amount)

    def the_account_balance_should_be_balance(self, balance = ""):
        balance = int(balance[1:])
        self.test.assertEqual(self.atm.current_balance(), balance)

    def the_atm_should_not_dispense_any_money(self):
        try:
            self.atm.withdraw(self.requested_amount)
            self.test.fail()
        except Exception as error:
            self.message = str(error)

    def the_atm_should_say_there_are_insufficient_funds(self):
        self.test.assertEqual(self.message, "insufficient funds")

    def the_card_is_disabled(self):
        try:
            self.atm.insert_card(Card('371520790975468', '', ''))
        except Exception as error:
            self.message = str(error)

    def the_atm_should_retain_the_card(self):
        self.atm.retain_card()

    def the_atm_should_say_the_card_has_been_retained(self):
        self.test.assertEqual(self.message, "retained card")

    def the_savings_account_balance_is_amount(self, amount = ""):
        amount = int(amount[1:])
        self.account.savings = amount

    def the_account_holder_transfers_amount_to_the_savings_account(self, amount = ""):
        self.atm.start_transaction(self.account)
        amount = int(amount[1:])
        self.atm.transfer_amount(amount)

    def the_savings_account_balance_should_be_amount(self, amount = ""):
        amount = int(amount[1:])
        self.test.assertEqual(self.atm.current_savings(), amount)
