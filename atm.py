# encoding: UTF-8
from cash_dispenser import CashDispenser
from card_reader import CardReader


class ATM(object):

    def __init__(self):
        self.cash_dispenser = CashDispenser()
        self.card_reader = CardReader()
        self.customer_account = None
        self.card = None

    def supply_cash_dispender(self, amount):
        self.cash_dispenser.supply(amount)

    def withdraw(self, amount):
        if amount > self.customer_account.balance:
            raise Exception("insufficient funds")

        self.cash_dispenser.withdraw(amount)
        self.customer_account.balance -= amount

    def start_transaction(self, account):
        self.customer_account = account

    def finish_transaction(self, account):
        self.customer_account = None

    def transfer_amount(self, amount):
        self.customer_account.balance -= amount
        self.customer_account.savings += amount

    def insert_card(self, card):
        track1, track2, track3 = self.card_reader.read(card)
        if not track1 or not track2 or not track3:
            raise Exception("retained card")

    def current_balance(self):
        return self.customer_account.balance

    def current_savings(self):
        return self.customer_account.savings

    def retain_card(self):
        self.card_reader.retain()

    def release_card(self):
        self.card_reader.release()
