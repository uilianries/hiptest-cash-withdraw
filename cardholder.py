# encoding: UTF-8
import collections

Cardholder = collections.namedtuple('Cardholder', 'name email')
