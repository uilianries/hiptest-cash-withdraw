# encoding: UTF-8

class CardReader(object):

    def __init__(self):
        self._lock = False

    def read(self, card):
        return (card.track1, card.track2, card.track3)

    def retain(self):
        self._lock = True

    def release(self):
        self._lock = False

    @property
    def is_locked(self):
        return self._lock
