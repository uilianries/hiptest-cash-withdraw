# encoding: UTF-8
import unittest
from actionwords import Actionwords

class TestAccountHolderWithdrawsCash(unittest.TestCase):
    def setUp(self):
        self.actionwords = Actionwords(self)

    def account_has_sufficient_funds(self, amount, ending_balance):
        # Tags: Priority:high
        # Given the account balance is "$100"
        self.actionwords.the_account_balance_is_balance(balance = "$100")
        # And the machine contains enough money
        self.actionwords.the_machine_contains_enough_money()
        # And the card is valid
        self.actionwords.the_card_is_valid()
        # When the Account Holder requests "<amount>"
        self.actionwords.the_account_holder_requests_amount(amount = amount)
        # Then the ATM should dispense "<amount>"
        self.actionwords.the_atm_should_dispense_amount(amount = amount)
        # And the account balance should be "<ending_balance>"
        self.actionwords.the_account_balance_should_be_balance(balance = ending_balance)
        # And the card should be returned
        self.actionwords.the_card_should_be_returned()

    def test_Account_has_sufficient_funds_withdraw_100_uidf7243ba7700f4c5cabad73bcebddcdef(self):
        self.account_has_sufficient_funds(amount = '$100', ending_balance = '$0')

    def test_Account_has_sufficient_funds_withdraw_50_uidd1df619cb95f49c8bd463ece96a54ded(self):
        self.account_has_sufficient_funds(amount = '$50', ending_balance = '$50')

    def test_Account_has_sufficient_funds_withdraw_20_uid2c1ef829fb0b49299773ca2680f40d6f(self):
        self.account_has_sufficient_funds(amount = '$20', ending_balance = '$80')



    def test_account_has_insufficient_funds_uid24f075e87b8a48208f95e568ed1aff2d(self):
        # Tags: Priority:high
        # Given the account balance is "$10"
        self.actionwords.the_account_balance_is_balance(balance = "$10")
        # And the card is valid
        self.actionwords.the_card_is_valid()
        # And the machine contains enough money
        self.actionwords.the_machine_contains_enough_money()
        # When the Account Holder requests "$20"
        self.actionwords.the_account_holder_requests_amount(amount = "$20")
        # Then the ATM should not dispense any money
        self.actionwords.the_atm_should_not_dispense_any_money()
        # And the ATM should say there are insufficient funds
        self.actionwords.the_atm_should_say_there_are_insufficient_funds()

    def test_card_has_been_disabled_uidb98f81f42c154c76b825a6ab1c51189a(self):
        # Tags: Priority:high
        # Given the card is disabled
        self.actionwords.the_card_is_disabled()
        # When the Account Holder requests "$10"
        self.actionwords.the_account_holder_requests_amount(amount = "$10")
        # Then the ATM should retain the card
        self.actionwords.the_atm_should_retain_the_card()
        # And the ATM should say the card has been retained
        self.actionwords.the_atm_should_say_the_card_has_been_retained()
