# encoding: UTF-8
import unittest
from actionwords import Actionwords

class TestAccountHolderTransfersCash(unittest.TestCase):
    def setUp(self):
        self.actionwords = Actionwords(self)

    def test_account_has_sufficient_funds_for_transferring_cash_uide6b34ea993e7485d97d8ef64d01d9b0b(self):
        # Tags: Priority:medium
        # Given the account balance is "$100"
        self.actionwords.the_account_balance_is_balance(balance = "$100")
        # And the savings account balance is "$1000"
        self.actionwords.the_savings_account_balance_is_amount(amount = "$1000")
        # And the card is valid
        self.actionwords.the_card_is_valid()
        # When the Account Holder transfers "$20" to the savings account
        self.actionwords.the_account_holder_transfers_amount_to_the_savings_account(amount = "$20")
        # And the ATM should dispense "$0"
        self.actionwords.the_atm_should_dispense_amount(amount = "$0")
        # And the account balance should be "$80"
        self.actionwords.the_account_balance_should_be_balance(balance = "$80")
        # And the savings account balance should be "$1020"
        self.actionwords.the_savings_account_balance_should_be_amount(amount = "$1020")
        # And the card should be returned
        self.actionwords.the_card_should_be_returned()
