# encoding: UTF-8

class CashDispenser(object):

    def __init__(self):
        self._total_amount = 0

    @property
    def total_amount(self):
        return self._total_amount

    def withdraw(self, amount):
        self._total_amount -= amount

    def supply(self, amount):
        self._total_amount += amount
