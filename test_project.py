# encoding: UTF-8
import unittest
from actionwords import Actionwords

class TestTestingCASHWITHDRAWALSampleN2(unittest.TestCase):
    def setUp(self):
        self.actionwords = Actionwords(self)

    def account_has_sufficient_funds(self, amount, ending_balance):
        # Tags: Priority:high
        # Given the account balance is "$100"
        self.actionwords.the_account_balance_is_balance(balance = "$100")
        # And the machine contains enough money
        self.actionwords.the_machine_contains_enough_money()
        # And the card is valid
        self.actionwords.the_card_is_valid()
        # When the Account Holder requests "<amount>"
        self.actionwords.the_account_holder_requests_amount(amount = amount)
        # Then the ATM should dispense "<amount>"
        self.actionwords.the_atm_should_dispense_amount(amount = amount)
        # And the account balance should be "<ending_balance>"
        self.actionwords.the_account_balance_should_be_balance(balance = ending_balance)
        # And the card should be returned
        self.actionwords.the_card_should_be_returned()

    def test_Account_has_sufficient_funds_withdraw_100(self):
        self.account_has_sufficient_funds(amount = '$100', ending_balance = '$0')

    def test_Account_has_sufficient_funds_withdraw_50(self):
        self.account_has_sufficient_funds(amount = '$50', ending_balance = '$50')

    def test_Account_has_sufficient_funds_withdraw_20(self):
        self.account_has_sufficient_funds(amount = '$20', ending_balance = '$80')



    def test_account_has_insufficient_funds(self):
        # Tags: Priority:high
        # Given the account balance is "$10"
        self.actionwords.the_account_balance_is_balance(balance = "$10")
        # And the card is valid
        self.actionwords.the_card_is_valid()
        # And the machine contains enough money
        self.actionwords.the_machine_contains_enough_money()
        # When the Account Holder requests "$20"
        self.actionwords.the_account_holder_requests_amount(amount = "$20")
        # Then the ATM should not dispense any money
        self.actionwords.the_atm_should_not_dispense_any_money()
        # And the ATM should say there are insufficient funds
        self.actionwords.the_atm_should_say_there_are_insufficient_funds()

    def test_account_has_sufficient_funds_for_transferring_cash(self):
        # Tags: Priority:medium
        # Given the account balance is "$100"
        self.actionwords.the_account_balance_is_balance(balance = "$100")
        # And the savings account balance is "$1000"
        self.actionwords.the_savings_account_balance_is_amount(amount = "$1000")
        # And the card is valid
        self.actionwords.the_card_is_valid()
        # When the Account Holder transfers "$20" to the savings account
        self.actionwords.the_account_holder_transfers_amount_to_the_savings_account(amount = "$20")
        # And the ATM should dispense "$0"
        self.actionwords.the_atm_should_dispense_amount(amount = "$0")
        # And the account balance should be "$80"
        self.actionwords.the_account_balance_should_be_balance(balance = "$80")
        # And the savings account balance should be "$1020"
        self.actionwords.the_savings_account_balance_should_be_amount(amount = "$1020")
        # And the card should be returned
        self.actionwords.the_card_should_be_returned()

    def test_card_has_been_disabled(self):
        # Tags: Priority:high
        # Given the card is disabled
        self.actionwords.the_card_is_disabled()
        # When the Account Holder requests "$10"
        self.actionwords.the_account_holder_requests_amount(amount = "$10")
        # Then the ATM should retain the card
        self.actionwords.the_atm_should_retain_the_card()
        # And the ATM should say the card has been retained
        self.actionwords.the_atm_should_say_the_card_has_been_retained()
